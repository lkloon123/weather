# Weather

### Installation
- clone this repo
- cd to `weather` folder
- run `composer install`
- run `npm install`
- run `npm run prod`
- copy `.env.example` to `.env`
- run `php artisan key:generate`
- run `php artisan serve`
- browse on `http://localhost:8000`
