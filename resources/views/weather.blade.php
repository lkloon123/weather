<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Weather</title>

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>

<div id="app" class="h-100">
    <weather-container></weather-container>
</div>

<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
